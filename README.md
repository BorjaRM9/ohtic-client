# StupidMarket

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.2.2.

## Requirements

![Requirements](https://gitlab.com/BorjaRM9/demos/-/raw/master/ohtic/recruting_tech_challenge.png)

## Demo

![Demo](https://gitlab.com/BorjaRM9/demos/-/raw/master/ohtic/stupid-market-demo.gif)
