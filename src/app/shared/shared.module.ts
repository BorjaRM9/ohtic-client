import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { BadgeModule } from 'primeng/badge';
import { ToastModule } from 'primeng/toast';

const primeNgModules = [
  BadgeModule,
  ToastModule
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ...primeNgModules
  ],
  exports: [
    CommonModule,
    ReactiveFormsModule,
    ...primeNgModules
  ]
})
export class SharedModule { }
