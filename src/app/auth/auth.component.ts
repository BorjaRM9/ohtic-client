import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { User } from '../core/models/user';
import { AuthService } from '../core/services/auth/auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit {

  formTemplate: string = 'login-form';

  constructor(private authService: AuthService, private router: Router, private messageService: MessageService) { }

  ngOnInit(): void { }

  register(user: User): void {
    this.authService.register(user).subscribe({
      next: () => { this.router.navigateByUrl('/login') },
      error: this.handleError.bind(this)
    });
  }

  login(user: User): void {
    this.authService.login(user).subscribe({
      next: () => { this.router.navigateByUrl('/market') },
      error: this.handleError.bind(this)
    });
  }

  setFormTemplate(templateName: string): void {
    this.formTemplate = templateName;
  }

  private handleError(error: any): void {
    this.messageService.add({ severity: 'error', summary: 'Error', detail: 'something bad happened' });
  }
}
