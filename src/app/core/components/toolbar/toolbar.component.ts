import { Component, OnDestroy, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { CartState } from '../../models/cart';
import { Product } from '../../models/product';
import { AuthService } from '../../services/auth/auth.service';
import { CartService } from '../../services/cart/cart.service';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit, OnDestroy {

  isCartLoaded: boolean = false;

  products: Product[] = [];

  isLoggedIn: boolean = false;

  currentRoute: string = '';

  private subscription: Subscription = new Subscription();

  constructor(private cartService: CartService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.subscription.add(
      this.cartService.cart.subscribe((cartState: CartState) => {
        this.isCartLoaded = cartState.loaded;
        this.products = cartState.products;
      })
    );

    this.subscription.add(
      this.router.events.subscribe(event => {
        if(event instanceof NavigationEnd){
          this.currentRoute = this.router.url;
          this.isLoggedIn = this.authService.isLoggedIn();
        }
      })
    );

  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  logout(): void {
    this.authService.logout();
  }

}
