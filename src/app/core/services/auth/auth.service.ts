import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { BehaviorSubject, Observable, shareReplay, tap } from 'rxjs';
import { User } from 'src/app/core/models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  userManagementApiUrl = '/usermanagement/v1';

  constructor(private http: HttpClient, @Inject('BASE_API_URL') private baseUrl: string) { }

  register(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}${this.userManagementApiUrl}/register`, user);
  }

  login(user: User): Observable<User> {
    return this.http.post<User>(`${this.baseUrl}/login`, user, { observe: 'response' })
      .pipe(
        tap(this.storeToken),
        shareReplay() // prevent the receiver of this Observable from accidentally
                      //triggering multiple POST requests due to multiple subscriptions
      );
  }

  logout(): void {
    localStorage.removeItem('auth-token');
    localStorage.removeItem('token-expiration');
  }

  isLoggedIn(): boolean {
    const now = Math.floor((new Date).getTime() / 1000);
    const expirationTime = localStorage.getItem('token-expiration');

    return expirationTime ? now <= +expirationTime : false;
  }

  isLoggedOut(): boolean {
    return !this.isLoggedIn();
  }

  private storeToken(response: any): void {
    const token = response.headers.get('Authorization');
    const expirationTime = (JSON.parse(atob(token.split('.')[1]))).exp;

    localStorage.setItem('auth-token', token);
    localStorage.setItem('token-expiration', JSON.stringify(expirationTime.valueOf()));
  }

}
