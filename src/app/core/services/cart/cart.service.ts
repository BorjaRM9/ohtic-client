import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CartState } from '../../models/cart';
import { Product } from '../../models/product';

@Injectable({
  providedIn: 'root',
})
export class CartService {

  constructor() {}

  private cartSubject = new BehaviorSubject<CartState>({ loaded: false, products: [] });

  private products: Product[] = [];

  cart = this.cartSubject.asObservable();

  addProduct(product: Product): void {
    this.products.push(product);
    this.cartSubject.next(<CartState> { loaded: true, products: this.products });
  }

  removeProduct(id: number): void {
    this.products = this.products.filter((item: Product) => item.productId !== id);
    this.cartSubject.next(<CartState> { loaded: false, products: this.products });
  }

}
