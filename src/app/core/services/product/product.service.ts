import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Product } from '../../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  productManagementApiUrl = '/productmanagement/v1';

  constructor(private http: HttpClient, @Inject('BASE_API_URL') private baseUrl: string) { }

  getProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.baseUrl}${this.productManagementApiUrl}/products`);
  }

}
