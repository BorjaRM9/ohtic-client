export interface Product {
  productId: number;
  name: string;
  description: string;
  image: string;
  seller: string;
  price: number;
  quantity: number;
}
