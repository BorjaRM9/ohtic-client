import { Component, OnInit } from '@angular/core';
import { Product } from '../core/models/product';
import { CartService } from '../core/services/cart/cart.service';
import { ProductService } from '../core/services/product/product.service';

@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }

}
