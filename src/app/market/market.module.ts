import { NgModule } from '@angular/core';

import { MarketRoutingModule } from './market-routing.module';
import { MarketComponent } from './market.component';
import { ProductCardComponent } from './products/product-card/product-card.component';
import { CartComponent } from './cart/cart.component';
import { ProductsComponent } from './products/products.component';
import { CartItemComponent } from './cart/cart-item/cart-item.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    MarketComponent,
    ProductCardComponent,
    CartComponent,
    ProductsComponent,
    CartItemComponent
  ],
  imports: [
    SharedModule,
    MarketRoutingModule
  ]
})
export class MarketModule { }
