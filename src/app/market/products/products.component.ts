import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/core/models/product';
import { AuthService } from 'src/app/core/services/auth/auth.service';
import { CartService } from 'src/app/core/services/cart/cart.service';
import { ProductService } from 'src/app/core/services/product/product.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];

  constructor(private productService: ProductService, private cartService: CartService, private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.productService.getProducts().subscribe(products => this.products = products);
  }

  addToCart(product: Product): void {

    if (this.authService.isLoggedIn()) {
      this.cartService.addProduct(product);
    } else {
      this.router.navigateByUrl('/login');
    }

  }

}
