import { Component, OnInit } from '@angular/core';
import { Product } from 'src/app/core/models/product';
import { CartService } from 'src/app/core/services/cart/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  cartProducts: Product[] = [];

  subtotal: number = 0;

  constructor(private cartService: CartService) { }

  ngOnInit(): void {

    this.cartService.cart.subscribe(cartState => {
      this.cartProducts = cartState.products;

      if (cartState.products.length > 0) {
        this.subtotal = this.cartProducts.map(product => product.price).reduce((acc, value) => acc + value);
      }

    });

  }

  removeFromCart(product: Product): void {
    this.cartService.removeProduct(product.productId);
  }

}
